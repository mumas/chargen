"""
Contains models for generating character level text.
"""

import numpy as np
from collections import Counter
from sklearn.naive_bayes import MultinomialNB
from sklearn.linear_model import LogisticRegression
from keras.models import Sequential
from keras.utils import np_utils, generic_utils
from keras.layers import Dense, Dropout, Activation
from keras.optimizers import SGD

class CharacterGenerator(object):

    @staticmethod
    def choice(chars, probs):
        return np.random.choice(chars, p=probs)

    @staticmethod
    def find_ngrams(input, n=2):
        """
        returns ngrams for the given list
        >>> Ngram().find_ngrams('abcd', n=2)
        ['*a', 'ab', 'bc', 'cd', 'd*']
        """

        candidate = '*'*(n-1)+input+'*'*(n-1)
        tuples = zip(*[candidate[i:] for i in range(n)])
        return [''.join(t) for t in tuples]

    def generate_next(self, input_string):
        '''Given the input string, generate next character'''
        raise NotImplemented

    def train(self, text):
        '''Train the model'''
        raise NotImplemented

    def get_name(self):
        raise NotImplemented


class Ngram(CharacterGenerator):

    model = None #n-gram counts
    chars = None #counts for characters
    temp = 1

    def __init__(self, n=2, temp=1):
        self.n = n
        self.temp = temp

    def __str__(self):
        return "{}-gram^{}".format(self.n-1, self.temp)

    def generate_next(self, input_string):
        '''
        Given the input string, generate next character

        >>> ngram = Ngram()
        >>> ngram.model={'a':Counter({'b': 60000, 'c': 1})}
        >>> ngram.generate_next('aaa')
        'b'
        '''

        no_key = False

        key = input_string[-(self.n-1):].lower()
        counts = self.model.get(key, self.chars)
        if counts == self.chars:
            no_key = True

        c2 = {k : v**self.temp for k,v in counts.items()}

        sum = np.sum(c2.values())
        probs = [(k, v/float(sum)) for k,v in c2.items()]
        if no_key:
            return CharacterGenerator.choice([c for c,w in probs], [w for c,w in probs]).upper()
        else:
            return CharacterGenerator.choice([c for c,w in probs], [w for c,w in probs])

    def train(self, text):
        '''Train the model'''
        self.model = self.count_ngrams(text, self.n)
        self.chars = Counter(text)

    def count_ngrams(self, text, n=2):
        """
        returns counts for each ngram
        >>> Ngram().count_ngrams('ab aab ab', n=2)
        {'a': Counter({'b': 3, 'a': 1}), ' ': Counter({'a': 2}), '*': Counter({'a': 1}), 'b': Counter({' ': 2, '*': 1})}

        """
        heads = {}
        ngrams = self.find_ngrams(text, n)
        for ngram in ngrams:
            head, tail = ngram[:-1], ngram[-1]
            tails = heads.get(head, list())
            tails.append(tail)
            heads[head] = tails

        ret = {}
        for head,tails in heads.items():
            cnt = Counter(tails)
            ret[head] = cnt

        return ret

class NHotOnes(CharacterGenerator):

    n = 3
    temp = 1
    clf = None

    def __init__(self, n=3, temp=1):
        self.n = n
        self.temp = temp

    def get_ch_dict(self, text):
        '''
        >>> NHotOnes().get_ch_dict('aba')
        {'a': 0, 'b': 1}
        '''
        return {c: i for i,c in enumerate(set(text))}

    def encode_input(self, ch_dict, input):
        '''
        encodes last n characters as array of hot-1 vectors

        >>> NHotOnes().encode_input({'a': 0, 'b': 1}, 'ab')
        array([ 1.,  0.,  0.,  1.])
        '''

        ret = []
        for c in input:
            hot1 = np.zeros(len(ch_dict))
            hot1[ch_dict[c]] = 1.0
            ret.append(hot1)

        return np.hstack(ret)

    def getModel(self):
        raise NotImplemented

    def train(self, text):
        self.ch_dict = self.get_ch_dict(text)
        self.rev_dict = {v: k for k,v in self.ch_dict.items()}
        examples = [text[i:i+self.n] for i in xrange(len(text)-self.n)]
        X = []
        Y = []
        for example in examples:
            head, tail = example[:-1], example[-1]
            X.append(self.encode_input(self.ch_dict, head))
            Y.append(self.ch_dict[tail])
        X = np.vstack(X)
        Y = np.array(Y)
        self.clf = self.getModel()
        self.clf.fit(X, Y)

    def generate_next(self, input_string):
        x = self.encode_input(self.ch_dict, input_string[-(self.n-1):])
        y = self.clf.predict_proba(x.reshape(1, -1))
        y = np.power(y[0], self.temp)
        y = y/sum(y)
        return self.rev_dict[self.choice(self.clf.classes_, y.tolist())]


class NaiveB(NHotOnes):

    def getModel(self):
        clf = MultinomialNB()
        return clf

    def __str__(self):
        return "{}-multinomNB".format(self.n-1)

class Logistic(NHotOnes):

    def getModel(self):
        clf = LogisticRegression(multi_class='multinomial', solver='lbfgs')
        return clf

    def __str__(self):
        return "{}-logistic".format(self.n-1)

class MLP(NHotOnes):

    def train(self, text):
        self.ch_dict = self.get_ch_dict(text)
        self.rev_dict = {v: k for k,v in self.ch_dict.items()}
        examples = [text[i:i+self.n] for i in xrange(len(text)-self.n)]
        X = []
        Y = []
        for example in examples:
            head, tail = example[:-1], example[-1]
            X.append(self.encode_input(self.ch_dict, head))
            Y.append(self.ch_dict[tail])

        X = np.matrix(X)
        Y = np_utils.to_categorical(np.matrix(np.array(Y)).T, nb_classes=len(self.ch_dict))

        model = Sequential()
        model.add(Dense(256, input_dim=X.shape[1], init='uniform'))
        model.add(Activation('sigmoid'))
        model.add(Dropout(0.1))
        model.add(Dense(256, init='uniform'))
        model.add(Activation('sigmoid'))
        model.add(Dropout(0.1))
        model.add(Dense(Y.shape[1], init='uniform'))
        model.add(Activation('softmax'))

        sgd = SGD(lr=0.1, decay=1e-6, momentum=0.9, nesterov=True)
        model.compile(loss='categorical_crossentropy',
                      optimizer=sgd)

        model.fit(X, Y,
                  nb_epoch=50 ,
                  batch_size=50,
                  show_accuracy=True)

        self.model = model

        preds = model.predict(np.atleast_2d(X[0]), verbose=0)[0]
        next_index = self.sample(preds, 0.2)
        next_char = self.rev_dict[next_index]
        print next_char

    def generate_next(self, input_string, temperature=0.2):
        x = np.array(self.encode_input(self.ch_dict, input_string[-(self.n-1):]))
        preds = self.model.predict(np.atleast_2d(x), verbose=0)[0]
        next_index = self.sample(preds, temperature)
        next_char = self.rev_dict[next_index]
        return next_char

    def sample(self, a, temperature=1.0):
        # helper function to sample an index from a probability array
        a = np.log(a) / temperature
        a = np.exp(a) / np.sum(np.exp(a))
        return np.argmax(np.random.multinomial(1, a, 1))

    def __str__(self):
        return 'MLP {}'.format(self.n)

if __name__ == "__main__":
    import doctest
    doctest.testmod()