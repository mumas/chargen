from models import Ngram, NaiveB, Logistic
from keras.datasets.data_utils import get_file

def read_text():
    path = get_file('nietzsche.txt', origin="https://s3.amazonaws.com/text-datasets/nietzsche.txt")
    text = open(path).read().lower()
    print('corpus length:', len(text))
    return text


from models import MLP

models = (

    MLP(10),
    Ngram(n=2, temp=0.1),
    Ngram(n=2),
    Ngram(n=2, temp=3),
    Ngram(n=3, temp=3),
    Ngram(n=4, temp=3),
    Ngram(n=5, temp=3),
    Ngram(n=8, temp=3),
    Ngram(n=11, temp=3),
    Ngram(n=21, temp=3),
    NaiveB(n=5, temp=2),
    Logistic(n=5, temp=2),
    Logistic(n=20, temp=2),

)

if __name__ == '__main__':
    alice = read_text()
    text = ' '.join(alice.split()).lower()
    print "The story is {} symbols long. Lets enjoy!".format(len(text))
    for m in models:
        m.train(text)
        start = ', own personal  experi'
        for _ in range(200):
            c = m.generate_next(start)
            start += c
        print m, start
        print "Is it the exact text from the input?", start[-50:] in text