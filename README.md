# Introduction #

LSTM implementation is taken from https://github.com/fchollet/keras/blob/master/examples/lstm_text_generation.py

# Needed Package Installation #

Install anaconda: http://docs.continuum.io/anaconda/install
Install Theano and Keras:

```
conda update conda
conda update --all
conda install numpy scipy
pip install git+git://github.com/Theano/Theano.git
pip install keras
```

# Run #
```
python generate.py
python lstm.py
```